const createStore = initialState => {
  const handler = {
    get(target, property, receiver) {
      const value = Reflect.get(target, property, receiver);

      if (typeof value === "object") {
        return new Proxy(value, handler);
      }

      return value || null;
    },
    set(target, property, value) {
      const newValue = Reflect.set(target, property, value);

      return newValue;
    }
  };

  return new Proxy(initialState, handler);
};

export default createStore;
