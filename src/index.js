console.log("started");

import "./styles/app.scss";

import createState from "./services/state";
import UserSearch from "./components/user-search";

window.bootstrapApp = () => {
  window.appState = createState({});
  window.rootApp = document.querySelector("#repositories-app");

  UserSearch(rootApp);
};

document.addEventListener("DOMContentLoaded", () => {
  bootstrapApp();
});
