import githubService from "../services/github";
import setLoading from "./loading";

const getLinkPagination = link => {
  if (!link.includes("last")) {
    return null;
  }

  const links = link
    .replace(/; rel="[a-z]+"/g, "")
    .replace(/[><]/g, "")
    .split(", ");

  if (links.length === 2) {
    return links[0];
  }

  return links[1];
};

const paginate = (rootContainer, type, link, renderComponent) => {
  const linkPagination = getLinkPagination(link);

  if (linkPagination === null) {
    rootContainer.innerHTML = "";

    return;
  }

  rootContainer.innerHTML = `
    <button>Carregar Mais</Button>
  `;

  const $button = rootContainer.querySelector("button");

  $button.addEventListener("click", async () => {
    setLoading(rootContainer);

    const {
      data,
      headers: { link }
    } = await githubService.get(linkPagination);

    appState[type] = [...appState[type], ...data];

    paginate(rootContainer, type, link, renderComponent);

    renderComponent();
  });
};

export default paginate;
