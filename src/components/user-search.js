"use strict";

import debounce from "lodash.debounce";
import githubService from "../services/github";
import userDetail from "./user-detail";
import setLoading from "./loading";

const DEFAULT_AVATAR = "http://via.placeholder.com/90x90/728";

const renderTemplate = () => `
  <div class="card" id="username-search">
    <img src="http://via.placeholder.com/90x90/728" class="avatar" />
    <input type="text" placeholder="Nome do usuário" />
    <button>Ver detalhes</button>
  </div>
`;

const searchUsername = debounce(async (event, rootElement, button) => {
  try {
    const user = event.target.value;

    const { data } = await githubService.get(`/users/${user}`);

    appState.user = data;

    rootElement.querySelector("img").src = data.avatar_url;
    button.disabled = false;
    button.textContent = "Ver detalhes";
  } catch (error) {
    appState.user = null;

    rootElement.querySelector("img").src = DEFAULT_AVATAR;
    button.disabled = true;
    button.textContent = "Usuário inexistente";
  }
}, 300);

const userLogin = rootElement => {
  rootElement.innerHTML = renderTemplate();

  const $usernameContainer = rootElement.querySelector("#username-search");
  const $input = $usernameContainer.querySelector("input");
  const $button = $usernameContainer.querySelector("button");

  $input.addEventListener("input", event =>
    searchUsername(event, $usernameContainer, $button)
  );

  $button.addEventListener("click", event => {
    setLoading(rootElement);

    setTimeout(() => userDetail(rootElement), 2000);
  });
};

export default userLogin;
