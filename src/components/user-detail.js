"use strict";

import githubService from "../services/github";
import UserRepositories from "./user-repositories";
import UserFavorites from "./user-favorites";
import setLoading from "./loading";
import paginateMore from "./paginate-more";

const rendertemplate = state => `
<div class="card card-detail" id="user-info">
<aside class="user-photo">
<img src="${state.user.avatar_url}" class="avatar" />

<a href="#">Visitar Perfil</a>
</aside>

<section class="user-info">
<div class="info">
<p>Repositórios: ${state.user.public_repos}</p>
<p>Seguidores: ${state.user.followers}</p>
<p>Seguindo: ${state.user.following}</p>
</div>

<div class="actions">
<button id="show-repositories">ver repositórios</button>
<button id="show-favorites">ver favoritos</button>
</div>
</section>

<div id="user-content"></div>
<div id="pagination"></div>

<button id="change-user">Trocar Usuário</button>
</div>
`;

const renderUserContent = async (type, userInfoContainer, componentRender) => {
  const $userContent = userInfoContainer.querySelector("#user-content");
  const $pagination = userInfoContainer.querySelector("#pagination");

  $pagination.innerHTML = "";

  const endpoints = {
    repositories: appState.user.repos_url,
    favorites: appState.user.starred_url.replace(/\{.*\}/g, "")
  };

  setLoading($userContent);

  const {
    data,
    headers: { link }
  } = await githubService.get(endpoints[type]);

  if (link) {
    paginateMore($pagination, type, link, () => componentRender($userContent));
  }

  appState[type] = data;

  componentRender($userContent);
};

const userDetail = rootElement => {
  rootElement.innerHTML = rendertemplate(appState);

  const $userInfoContainer = rootElement.querySelector("#user-info");
  const $buttonShowRepositories = $userInfoContainer.querySelector(
    "#show-repositories"
  );
  const $buttonShowFavorites = $userInfoContainer.querySelector(
    "#show-favorites"
  );
  const $changeUser = $userInfoContainer.querySelector("#change-user");

  $buttonShowRepositories.addEventListener("click", () => {
    renderUserContent("repositories", $userInfoContainer, UserRepositories);
  });

  $buttonShowFavorites.addEventListener("click", () => {
    renderUserContent("favorites", $userInfoContainer, UserFavorites);
  });

  $changeUser.addEventListener("click", () => {
    bootstrapApp();
  });
};

export default userDetail;
