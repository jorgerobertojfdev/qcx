"use strict";

const renderTemplate = state => `
  <ul>
    ${state
      .map(
        item =>
          `<li>${item.name} <a target="_blank" href="${item.html_url}">Ver Repo</a></li>`
      )
      .join("")}
  <ul>
`;

const userDetail = rootElement => {
  rootElement.innerHTML = renderTemplate(appState.favorites);
};

export default userDetail;
